import type { User } from '@/types/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('email', user.email)
  fromData.append('fullName', user.fullName)
  fromData.append('gender', user.gender)
  fromData.append('password', user.email)
  fromData.append('roles', JSON.stringify(user.roles))
  if(user.files && user.files.length > 0) fromData.append('file', user.files[0])
  return http.post('/users', fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateUser(user: User & { files: File[] }) {
  const fromData = new FormData()
  fromData.append('email', user.email)
  fromData.append('fullName', user.fullName)
  fromData.append('gender', user.gender)
  fromData.append('password', user.email)
  fromData.append('roles', JSON.stringify(user.roles))
  if(user.files && user.files.length > 0) fromData.append('file', user.files[0])
  return http.post(`/users/${user.id}`, fromData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getUsers() {
  return http.get('/users')
}

export default { addUser, updateUser, delUser, getUser, getUsers }
